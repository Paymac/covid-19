import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

var lastPage;

String myEmail;
int unknownColor=0xFF95a5a6;
int healthyColor=0xFF27ae60;
int prospectColor=0xFFf39c12;
int infectedColor=0xFFe74c3c;
int immuneColor=0xFF8e44ad;


List<String> devicesAddresses=[];

loading(BuildContext context){
  showGeneralDialog(
    context: context,
    barrierColor: Colors.black38.withOpacity(0.6), // Background color
    barrierDismissible: false,
    transitionDuration: Duration(milliseconds: 400), // How long it takes to popup dialog after button click
    pageBuilder: (_, __, ___) {
      // Makes widget fullscreen
      return SizedBox.expand(
          child: Container(
              alignment: Alignment.center,
              child: CupertinoTheme(
                data: CupertinoThemeData(brightness: Brightness.dark),
                child:  CupertinoActivityIndicator(),
              )
          )
      );
    },
  );
}

Future<bool> checkInternet()async{
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }else{
      return false;
    }
  } on SocketException catch (_) {
    return false;
  }
}

var questionnaireTime;