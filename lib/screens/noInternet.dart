import 'package:covid19/global.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NoInternet extends StatefulWidget{
  @override
  StateClass createState() => StateClass();
}

class StateClass extends State<NoInternet>{

  bool loading=false;

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DoubleBackToCloseApp(
        child: SizedBox.expand(
            child: Container(
                color: Color(0xFFFAFAFA),
                alignment: Alignment.center,
                child: Center(child: Column(crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,mainAxisSize: MainAxisSize.min,children: [
                  Icon(Icons.wifi_off,color: Colors.green,size: 150,),
                  SizedBox(height: 5,),
                  Text("No Internet",style: TextStyle(color: Colors.green,fontSize: 16),),
                  loading? Container(
                    margin: EdgeInsets.all(15),
                    child: CircularProgressIndicator(),
                  )  : IconButton(iconSize: 50,icon: Icon(Icons.refresh), onPressed: (){
                    setState(() {
                      loading=true;
                    });

                    checkInternet().then((value) => {

                      if(value){
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>lastPage))
                      }else{
                        setState(() {
                          loading=false;
                        })
                      }

                    });

                  })
                ],),)
            )
        ),
        snackBar: SnackBar(
          content: Text('Tap back again to exit',textAlign: TextAlign.center,),
        ),
      ),
    );
  }
}