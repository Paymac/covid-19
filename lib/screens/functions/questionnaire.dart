import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import '../../global.dart';
import 'edit_user.dart';
popupQuestionnaire(BuildContext context){
  int q1=-1,q2=-1,q3=-1,q4=-1,q5=-1,q6=-1,q7=-1,q8=-1,q9=-1,q10=-1;
  showDialog(
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return AlertDialog(
            insetPadding: EdgeInsets.all(10),
            content: SingleChildScrollView(child: Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
              Text("Covid-19 Questionnaire",style: TextStyle(color: Colors.green,fontSize: 20,fontWeight: FontWeight.bold),),
              Divider(),
              SizedBox(height: 10,),

              Text("With an oral temperature of 38.1 ° C (100.6 ° F) or higher, do you feel feverish, have flu-like chills or fever?",
                style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",
                  style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q1, onChanged: (v){
                  setState(() => q1 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",
                  style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q1, onChanged: (v){
                  setState(() => q1 = v);
                })),
              ],),

              Text("Have you had a sudden loss of smell with or without loss of taste without nasal congestion (stuffy nose)?",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q2, onChanged: (v){
                  setState(() => q2 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q2, onChanged: (v){
                  setState(() => q2 = v);
                })),
              ],),
              Text("Do you also have a cough or have lately gotten worse with chronic cough?",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q3, onChanged: (v){
                  setState(() => q3 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q3, onChanged: (v){
                  setState(() => q3 = v);
                })),
              ],),
              Text("Are you developing breathing difficulties or shortness of breath?",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q4, onChanged: (v){
                  setState(() => q4 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q4, onChanged: (v){
                  setState(() => q4 = v);
                })),
              ],),
              Text("Have you got a sore throat?",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q5, onChanged: (v){
                  setState(() => q5 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q5, onChanged: (v){
                  setState(() => q5 = v);
                })),
              ],),
              Text("Stomach ache",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q6, onChanged: (v){
                  setState(() => q6 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q6, onChanged: (v){
                  setState(() => q6 = v);
                })),
              ],),
              Text("Nausea or vomiting",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q7, onChanged: (v){
                  setState(() => q7 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q7, onChanged: (v){
                  setState(() => q7 = v);
                })),
              ],),
              Text("Diarrheal",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q8, onChanged: (v){
                  setState(() => q8 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q8, onChanged: (v){
                  setState(() => q8 = v);
                })),
              ],),
              Text("Unusually intense fatigue for no obvious reason",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q9, onChanged: (v){
                  setState(() => q9 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q9, onChanged: (v){
                  setState(() => q9 = v);
                })),
              ],),
              Text("Significant loss of appetite",style: TextStyle(fontSize: 14),),
              Row(children: [
                Expanded(child: RadioListTile(title: Text("Yes",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 1, groupValue: q10, onChanged: (v){
                  setState(() => q10 = v);
                })),
                Expanded(child: RadioListTile(title: Text("No",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),value: 0, groupValue: q10, onChanged: (v){
                  setState(() => q10 = v);
                })),
              ],),
              SizedBox(height: 20,),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [
                Material(
                  elevation: 1,
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                      colors: [Colors.white, Colors.white],
                    ),),
                    child: ElevatedButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                          padding: MaterialStateProperty.all(
                              EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                          ),
                          backgroundColor: MaterialStateProperty.all(
                              Colors.transparent
                          ),
                          elevation: MaterialStateProperty.all(0)
                      ),
                      child: Text("Cancel",style: TextStyle(color: Colors.green),),
                    ),
                  ),
                ),
                Material(
                  elevation: 1,
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                      colors: [Colors.green[300], Colors.green],
                    ),),
                    child: ElevatedButton(
                      onPressed: (){
                        if(q1==-1 || q2==-1 || q3==-1 || q4==-1 || q5==-1 || q6==-1 || q7==-1 || q8==-1 || q9==-1 || q10==-1){

                          Flushbar(
                            flushbarPosition: FlushbarPosition.TOP,
                            flushbarStyle: FlushbarStyle.GROUNDED,
                            backgroundColor: Color(0xFFc0392b),
                            messageText: Text("Please answer all questions",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
                            duration:  Duration(seconds: 3),
                          )..show(context);

                        }else{


                          int infectedCount=0;
                          if(q1==1){
                            infectedCount=infectedCount+1;
                          }else if(q2==1){
                            infectedCount=infectedCount+1;
                          }else if(q3==1){
                            infectedCount=infectedCount+1;
                          }else if(q4==1){
                            infectedCount=infectedCount+1;
                          }else if(q5==1){
                            infectedCount=infectedCount+1;
                          }

                          int prospectCount=0;
                          if(q6==1){
                            prospectCount=prospectCount+1;
                          }else if(q7==1){
                            prospectCount=prospectCount+1;
                          }else if(q8==1){
                            prospectCount=prospectCount+1;
                          }else if(q9==1){
                            prospectCount=prospectCount+1;
                          }else if(q10==1){
                            prospectCount=prospectCount+1;
                          }

                          if(infectedCount>=1&&prospectCount>=1){
                            //infected
                            editUser(context, myEmail, 3);

                          }else if(infectedCount>=1||prospectCount>=2){
                            //prospect
                            editUser(context, myEmail, 2);
                          }else{
                            editUser(context, myEmail, 1);
                            Navigator.pop(context);
                          }

                        }
                      },
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                          padding: MaterialStateProperty.all(
                              EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                          ),
                          backgroundColor: MaterialStateProperty.all(
                              Colors.transparent
                          ),
                          elevation: MaterialStateProperty.all(0)
                      ),
                      child: Text("Submit"),
                    ),
                  ),
                ),
              ],),
              SizedBox(height: 20,),
            ],),),
          );
        },
      );
    },
  );
}