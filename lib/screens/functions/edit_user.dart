import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
final databaseReference = FirebaseFirestore.instance;
void editUser(BuildContext context,String id,int status) async{
  if(status==3){
    databaseReference.collection('users').doc(id).update({"status":status,"questionnaire_time":Timestamp.now()}).then((result){
      Navigator.of(context).pop();
    }).catchError((onError){
      print("onError");
      Navigator.of(context).pop();
    });
  }else{
    databaseReference.collection('users').doc(id).update({"status":status}).then((result){
      Navigator.of(context).pop();
    }).catchError((onError){
      print("onError");
      Navigator.of(context).pop();
    });
  }
}