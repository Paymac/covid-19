import 'package:encrypt/encrypt.dart';
final key = Key.fromLength(32);
final iv = IV.fromLength(8);
final encrypter = Encrypter(Salsa20(key));

encrypt(String text){
  return encrypter.encrypt(text, iv: iv).base64;
}

decrypt(String encrypted){
  return encrypter.decrypt64(encrypted, iv: iv);
}

