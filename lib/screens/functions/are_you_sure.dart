import 'package:covid19/global.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../login.dart';
areYouSure(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text("Are you sure you want to logout?"),
        actions: [
          TextButton(
            child: Text("No"),
            onPressed:  () {
              Navigator.pop(context);
            },
          ),
          TextButton(
            child: Text("Yes"),
            onPressed:  () async{
              myEmail=null;
              devicesAddresses.clear();
              questionnaireTime=null;
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Login()));
            },
          ),
        ],
      );
    },
  );
}