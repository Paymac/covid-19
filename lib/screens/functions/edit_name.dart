import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:covid19/widgets/textformfield.dart';
import 'package:flutter/material.dart';
popupEditName(String id ,String name,TextEditingController nameTextEditingController,BuildContext context){
  nameTextEditingController.text=name;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Color(0xFFFAFAFA),
          scrollable: true,
          content: SingleChildScrollView(child: Column(children: [
            Text("Edit Name",style: TextStyle(color: Colors.green),),
            SizedBox(height: 10,),
            CustomTextField(
              keyboardType: TextInputType.text,
              icon: Icons.person,
              hint: "Name",
              textEditingController: nameTextEditingController,
            ),
            SizedBox(height: 20,),
            Material(
              elevation: 1,
              borderRadius: BorderRadius.all(Radius.circular(100)),
              child: Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),),
                child: ElevatedButton(
                  onPressed: (){
                    if(nameTextEditingController.text.isNotEmpty){
                      editName(id,nameTextEditingController,context);
                    }
                  },
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                      padding: MaterialStateProperty.all(
                          EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                      ),
                      backgroundColor: MaterialStateProperty.all(
                          Colors.transparent
                      ),
                      elevation: MaterialStateProperty.all(0)
                  ),
                  child: Text("Edit"),
                ),
              ),
            ),
          ],),),
        );
      });
}

void editName(String id,TextEditingController nameTextEditingController,BuildContext context) async {
  final databaseReference = FirebaseFirestore.instance;
  await databaseReference.collection("users")
      .doc(id)
      .update({
    'name': nameTextEditingController.text,
  });
  Navigator.pop(context);
}