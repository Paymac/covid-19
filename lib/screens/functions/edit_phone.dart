import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:covid19/widgets/textformfield.dart';
import 'package:flutter/material.dart';
popupEditPhone(String id ,String phone,TextEditingController phoneTextEditingController,BuildContext context){
  phoneTextEditingController.text=phone;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Color(0xFFFAFAFA),
          scrollable: true,
          content: SingleChildScrollView(child: Column(children: [
            Text("Edit Phone",style: TextStyle(color: Colors.green),),
            SizedBox(height: 10,),
            CustomTextField(
              keyboardType: TextInputType.phone,
              icon: Icons.phone_android,
              hint: "Phone",
              textEditingController: phoneTextEditingController,
            ),
            SizedBox(height: 20,),
            Material(
              elevation: 1,
              borderRadius: BorderRadius.all(Radius.circular(100)),
              child: Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),),
                child: ElevatedButton(
                  onPressed: (){
                    if(phoneTextEditingController.text.isNotEmpty){
                      editPhone(id,phoneTextEditingController,context);
                    }
                  },
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                      padding: MaterialStateProperty.all(
                          EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                      ),
                      backgroundColor: MaterialStateProperty.all(
                          Colors.transparent
                      ),
                      elevation: MaterialStateProperty.all(0)
                  ),
                  child: Text("Edit"),
                ),
              ),
            ),
          ],),),
        );
      });
}
void editPhone(String id,TextEditingController phoneTextEditingController,BuildContext context) async {
  final databaseReference = FirebaseFirestore.instance;
  await databaseReference.collection("users")
      .doc(id)
      .update({
    'phone': phoneTextEditingController.text,
  });
  Navigator.pop(context);
}