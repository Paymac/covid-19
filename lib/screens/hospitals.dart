import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Hospitals extends StatefulWidget{
  @override
  StateClass createState() => StateClass();
}

class StateClass extends State<Hospitals>{

  static Future<void> openMap(String latitude, String longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            elevation: 1,
            centerTitle: true,
            title: Text('Hospitals',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),
              ),
            ),
          ),
        ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('hospitals').snapshots(),
        builder: (_, AsyncSnapshot<QuerySnapshot> snapshot){
          var hospitals = snapshot.data?.docs ?? [];

          return hospitals.length==0? Container(alignment: Alignment.center,width: double.infinity,height: double.infinity,child: Text("No hospitals in database",style: TextStyle(color: Colors.black45),),) : ListView.builder(
            itemCount: hospitals.length,
            itemBuilder: (context,index){
              return Card(
                elevation: 1,
                child: ListTile(
                  onTap: (){
                    openMap(hospitals[index]['latitude'].toString(), hospitals[index]['longitude'].toString());
                  },
                  leading: Icon(Icons.store),
                  title: Text(hospitals[index]['title']),
                  trailing: Icon(Icons.location_on),
                ),
              );
            },
          );
        },
      ),
    );
  }
}