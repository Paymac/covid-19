import 'package:covid19/global.dart';
import 'package:covid19/screens/functions/encrypt.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/custom_shape.dart';
import 'package:covid19/widgets/textformfield.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'loading.dart';
import 'noInternet.dart';

class Register extends StatefulWidget {
  @override
  SignUpScreenState createState() => SignUpScreenState();
}

class SignUpScreenState extends State<Register> {
  bool checkBoxValue = false;
  bool passwordHidden1=true;
  bool passwordHidden2=true;
  TextEditingController nameTextEditingController=new TextEditingController();
  TextEditingController bluetoothTextEditingController=new TextEditingController();
  TextEditingController govTextEditingController=new TextEditingController();
  TextEditingController emailTextEditingController=new TextEditingController();
  TextEditingController phoneTextEditingController=new TextEditingController();
  TextEditingController password1TextEditingController=new TextEditingController();
  TextEditingController password2TextEditingController=new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  saveEmail(String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('email', email);
  }

  @override
  Widget build(BuildContext context) {

    final databaseReference = FirebaseFirestore.instance;

    void register() async {
      await databaseReference.collection("users")
          .doc(emailTextEditingController.text)
          .set({
        'name': nameTextEditingController.text,
        'questionnaire_time':null,
        'bluetooth': bluetoothTextEditingController.text.trim().toLowerCase(),
        'governmentID' : govTextEditingController.text,
        'email': emailTextEditingController.text.toLowerCase(),
        'phone': phoneTextEditingController.text,
        'password': encrypt(password1TextEditingController.text),
        'status': 1
      });
      saveEmail(emailTextEditingController.text.toLowerCase());
      myEmail=emailTextEditingController.text.toLowerCase();
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>Loading()), (route) => false);
    }

    void validation(){
      bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailTextEditingController.text);
      if(nameTextEditingController.text.isEmpty){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please enter your name",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(bluetoothTextEditingController.text.isEmpty){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please enter your bluetooth address",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(govTextEditingController.text.isEmpty){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please enter your government ID",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(emailTextEditingController.text.isEmpty){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please enter your email",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(!emailValid){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Wrong email form",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(phoneTextEditingController.text.isEmpty){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please enter your phone",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(password1TextEditingController.text.isEmpty){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please enter your password",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(password2TextEditingController.text.isEmpty){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please confirm your password",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(password1TextEditingController.text!=password2TextEditingController.text){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Password confirmation doesnt match",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else if(!checkBoxValue){
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Please accept our terms and conditions",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }else{
        loading(context);
        databaseReference.collection("users").where(FieldPath.documentId,isEqualTo: emailTextEditingController.text).get()
            .then((event) {
          if (event.docs.isNotEmpty) {
            Navigator.pop(context);
            Flushbar(
              flushbarPosition: FlushbarPosition.TOP,
              flushbarStyle: FlushbarStyle.GROUNDED,
              backgroundColor: Color(0xFFc0392b),
              messageText: Text("Email already exists",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
              duration:  Duration(seconds: 3),
            )..show(context);
          }else{
            checkInternet().then((value) => {
              if(value){
                register()
              }else{
                lastPage=Register(),
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>NoInternet()))
              }
            });
          }
        });
      }
    }

    return Material(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              clipShape(),
              form(),
              acceptTermsTextRow(),
              SizedBox(height: 20),
              Material(
                elevation: 1,
                borderRadius: BorderRadius.all(Radius.circular(100)),
                child: Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                    colors: [Colors.green[300], Colors.green],
                  ),),
                  child: ElevatedButton(
                    onPressed: (){
                      validation();
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                        padding: MaterialStateProperty.all(
                            EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                        ),
                        backgroundColor: MaterialStateProperty.all(
                            Colors.transparent
                        ),
                        elevation: MaterialStateProperty.all(0)
                    ),
                    child: Text("SIGN UP"),
                  ),
                ),
              ),
              SizedBox(height: 20,)
            ],
          ),
        ),
      ),
    );
  }

  Widget clipShape() {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.75,
          child: ClipPath(
            clipper: CustomShapeClipper(),
            child: Container(
              height: 220,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: ClipPath(
            clipper: CustomShapeClipper2(),
            child: Container(
              height: 170,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),
              ),
            ),
          ),
        ),
        Positioned.fill(child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: 150,
            height: 150,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 0.0,
                      color: Colors.black26,
                      offset: Offset(1.0, 10.0),
                      blurRadius: 20.0),
                ],
                color: Colors.white,
                shape: BoxShape.circle,
                image: DecorationImage(image: AssetImage('assets/images/logo.png'))
            ),
          ),
        ),),
        AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
        )
      ],
    );
  }

  popup(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            content: Padding(
              padding: const EdgeInsets.all(5),
              child: Column(children: [
                Row(children: [
                  Text("ANDROID : ",style: TextStyle(color: Colors.black45,fontWeight: FontWeight.bold,fontSize: 13),),
                  Text("Settings => About phone",style: TextStyle(color: Colors.black45,fontSize: 13),),
                ],),
                Text("=> Status => Bluetooth address",style: TextStyle(color: Colors.black45,fontSize: 13),),
                SizedBox(height: 5,),
                Divider(),
                SizedBox(height: 5,),
                Row(children: [
                  Text("IPHONE : ",style: TextStyle(color: Colors.black45,fontWeight: FontWeight.bold,fontSize: 13),),
                  Text("Settings => General",style: TextStyle(color: Colors.black45,fontSize: 13),),
                ],),
                Text("=> About => Bluetooth",style: TextStyle(color: Colors.black45,fontSize: 13),),
              ],),
            ),
          );
        });
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 40),
      child: Form(
        child: Column(
          children: <Widget>[
            nameTextFormField(),
            SizedBox(height: 20),
            Stack(children: [
              bluetoothTextFormField(),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: IconButton(onPressed: (){
                    popup();
                  },icon: Icon(Icons.info_outline,size: 20,color: Colors.black54,),),
                ),
              )
            ],),
            SizedBox(height: 20),
            govIDTextFormField(),
            SizedBox(height: 20),
            emailTextFormField(),
            SizedBox(height: 20),
            phoneTextFormField(),
            SizedBox(height: 20),
            Stack(children: [
              CustomTextField(
                keyboardType: TextInputType.text,
                obscureText: passwordHidden1,
                icon: Icons.lock,
                hint: "Password",
                textEditingController: password1TextEditingController,
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: IconButton(onPressed: (){

                    if(passwordHidden1){
                      setState(() {
                        passwordHidden1=false;
                      });
                    }else{
                      setState(() {
                        passwordHidden1=true;
                      });
                    }

                  },icon: Icon(!passwordHidden1 ? Icons.visibility_outlined : Icons.visibility_off_outlined,size: 20,color: Colors.black54,),),
                ),
              )
            ],),
            SizedBox(height: 20),
            Stack(children: [
              CustomTextField(
                keyboardType: TextInputType.text,
                obscureText: passwordHidden2,
                icon: Icons.lock,
                hint: "Confirm password",
                textEditingController: password2TextEditingController,
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: IconButton(onPressed: (){

                    if(passwordHidden2){
                      setState(() {
                        passwordHidden2=false;
                      });
                    }else{
                      setState(() {
                        passwordHidden2=true;
                      });
                    }

                  },icon: Icon(!passwordHidden2 ? Icons.visibility_outlined : Icons.visibility_off_outlined,size: 20,color: Colors.black54,),),
                ),
              )
            ],)
          ],
        ),
      ),
    );
  }

  Widget nameTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      icon: Icons.person,
      hint: "Name",
      textEditingController: nameTextEditingController,
    );
  }

  Widget bluetoothTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      icon: Icons.bluetooth,
      hint: "Bluetooth address",
      textEditingController: bluetoothTextEditingController,
    );
  }

  Widget govIDTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.number,
      icon: Icons.credit_card,
      hint: "Government ID",
      textEditingController: govTextEditingController,
    );
  }

  Widget emailTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.emailAddress,
      icon: Icons.email,
      hint: "Email",
      textEditingController: emailTextEditingController,
    );
  }

  Widget phoneTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.number,
      icon: Icons.phone_android,
      hint: "Mobile Number",
      textEditingController: phoneTextEditingController,
    );
  }

  Widget acceptTermsTextRow() {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Checkbox(
              activeColor: Colors.green[300],
              value: checkBoxValue,
              onChanged: (bool newValue) {
                setState(() {
                  checkBoxValue = newValue;
                });
              }),
          Text(
            "I accept all terms and conditions",
            style: TextStyle(fontWeight: FontWeight.w400, fontSize: 12),
          ),
        ],
      ),
    );
  }
}