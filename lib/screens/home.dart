import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:covid19/global.dart';
import 'package:covid19/screens/hospitals.dart';
import 'package:dio/dio.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_scan_bluetooth/flutter_scan_bluetooth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'functions/are_you_sure.dart';
import 'functions/edit_bluetooth.dart';
import 'functions/edit_name.dart';
import 'functions/edit_phone.dart';
import 'functions/questionnaire.dart';
class Home extends StatefulWidget{
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>{

  SharedPreferences prefs;
  final databaseReference = FirebaseFirestore.instance;
  bool scanning=true;
  final controller = PageController(initialPage: 0);
  int currentIndex=0;
  TextEditingController nameTextEditingController=TextEditingController();
  TextEditingController bluetoothTextEditingController=TextEditingController();
  TextEditingController phoneTextEditingController=TextEditingController();

  FlutterLocalNotificationsPlugin fltrNotification;

  @override
  void initState() {
    super.initState();

    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        requestSoundPermission: false,
        requestBadgePermission: false,
        requestAlertPermission: false
    );
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: false);
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    fltrNotification = new FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initializationSettings,
        onSelectNotification: notificationSelected);

    startBluetoothScan();

    checkInternet();

  }

  Future notificationSelected(String payload) async {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text("Notification : $payload"),
      ),
    );
  }

  Future _showNotification(String title,String body) async {

    const AndroidNotificationDetails androidPlatformChannelSpecifics =
    AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.max,
        priority: Priority.high,
        showWhen: false);
    const NotificationDetails platformChannelSpecifics =
    NotificationDetails(android: androidPlatformChannelSpecifics);
    await fltrNotification.show(
        0, title, body, platformChannelSpecifics,
        payload: 'item x');
  }

  String cases="",todayCases="",deaths="",todayDeaths="",recovered="",active="",critical="",tests="",affectedCountries="";

  void getHttp() async {
    loading(context);
    var formatter = NumberFormat('###,###,000');
    try {
      var response = await Dio().get('https://corona.lmao.ninja/v2/all');
      setState(() {
        cases=formatter.format(response.data['cases']).toString();
        todayCases=formatter.format(response.data['todayCases']).toString();
        deaths=formatter.format(response.data['deaths']).toString();
        todayDeaths=formatter.format(response.data['todayDeaths']).toString();
        active=formatter.format(response.data['active']).toString();
        critical=formatter.format(response.data['critical']).toString();

        recovered=formatter.format(response.data['recovered']).toString();
        tests=formatter.format(response.data['tests']).toString();
        affectedCountries=formatter.format(response.data['affectedCountries']).toString();
      });
    } catch (e) {
      print(e);
    }
    Navigator.pop(context);
  }

  FlutterScanBluetooth _bluetooth = FlutterScanBluetooth();
  void startBluetoothScan()async{
    prefs = await SharedPreferences.getInstance();
    if(prefs.getStringList("devicesAddresses")!=null){
      setState(() {
        devicesAddresses=prefs.getStringList("devicesAddresses");
      });
    }

    _bluetooth.devices.listen((device) {
      String _data = device.address.trim().toLowerCase();
      setState(() {
        devicesAddresses.add(_data);
        devicesAddresses=devicesAddresses.toSet().toList();
      });
      prefs.setStringList("devicesAddresses", devicesAddresses);
      print(_data);

    });

    try {
      await _bluetooth.requestPermissions();
      await _bluetooth.startScan();

    } on PlatformException catch (e) {
      debugPrint("-----------------------"+e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: Color(0xFFf1f2f6),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 1,
          centerTitle: true,
          title: currentIndex==0? Center(child: Column(mainAxisSize: MainAxisSize.min,children: [
            Text('Covid 19',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),),
            Text('tracking platform',style: TextStyle(color: Colors.white,fontSize: 12),),
          ],),)
              : currentIndex==1? Text('Profile',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),)
              : currentIndex==3? Text('Statistics',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),)
              : currentIndex==4? Text('Instructions',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),)
              : Text('',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.green[300], Colors.green],
              ),
            ),
          ),
        ),
      ),
      body: DoubleBackToCloseApp(
        child: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: controller,
          children: [

            StreamBuilder(
              stream: FirebaseFirestore.instance.collection('users').snapshots(),
              builder: (_, AsyncSnapshot<QuerySnapshot> snapshot){
                var items = snapshot.data?.docs ?? [];
                int healthyCount=0,prospectCount=0,infectedCount=0,unknownCount=0,immuneCount=0;
                for(int i=0;i<devicesAddresses.length;i++){
                  bool exist=false;
                  for(int j=0;j<items.length;j++){
                    if(devicesAddresses[i]==items[j]['bluetooth']){
                      exist=true;
                      if(items[j]['status']==0){
                        unknownCount=unknownCount+1;
                      }else if(items[j]['status']==1){
                        healthyCount=healthyCount+1;
                      }else if(items[j]['status']==2){
                        prospectCount=prospectCount+1;
                      }else if(items[j]['status']==3){
                        infectedCount=infectedCount+1;
                      }else if(items[j]['status']==4){
                        immuneCount=immuneCount+1;
                      }
                    }
                  }
                  if(!exist){
                    unknownCount=unknownCount+1;
                  }
                }


                if(snapshot.connectionState==ConnectionState.active){

                  if(infectedCount>0){
                    _showNotification("Covid 19 tracking platform", "You have encountered infected cases");
                  }else if(prospectCount>0){
                    _showNotification("Covid 19 tracking platform", "You have encountered prospect cases");
                  }

                }

                int myStatus=0;
                for(int i=0;i<items.length;i++){
                  if(items[i].id==myEmail){
                    myStatus=items[i]['status'];
                    questionnaireTime=items[i]['questionnaire_time'];
                  }
                }

                if(myStatus!=3){
                  if(myStatus==0 || myStatus==1){
                    if(infectedCount>=1){
                      databaseReference.collection('users').doc(myEmail).update({"status":2}).then((result){
                      }).catchError((onError){
                        print("onError");
                      });
                    }
                  }
                }

                return ListView(children: [
                  Column(children: [

                    myStatus==3? Column(children: [
                      Card(elevation: 11,color: Color(infectedColor),child: ListTile(contentPadding: EdgeInsets.all(10),leading: IconButton(onPressed: null, icon: Icon(Icons.warning,color: Colors.white,)),title: Text("You are infected you need immediate isolation please head to the nearest hospital",style: TextStyle(color: Colors.white,fontSize: 15),)),),
                      SizedBox(height: 10,),
                      Material(
                        elevation: 1,
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        child: Container(
                          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                            colors: [Colors.green[300], Colors.green],
                          ),),
                          child: ElevatedButton(
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>Hospitals()));
                            },
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                                ),
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.transparent
                                ),
                                elevation: MaterialStateProperty.all(0)
                            ),
                            child: Text("Nearest hospitals"),
                          ),
                        ),
                      ),
                    ],)
                        :

                    infectedCount!=0?  Column(children: [
                      Card(elevation: 11,color: Color(infectedColor),child: ListTile(contentPadding: EdgeInsets.all(10),leading: IconButton(onPressed: null, icon: Icon(Icons.warning,color: Colors.white,)),title: Text("You have encountered infected cases you need immediate isolation please take the questionnaire then head to the nearest hospital",style: TextStyle(color: Colors.white,fontSize: 15),),trailing: IconButton(onPressed: (){
                        popupQuestionnaire(context);
                      }, icon: Icon(FontAwesomeIcons.stethoscope,color: Colors.white,)),),),
                      SizedBox(height: 10,),
                      Material(
                        elevation: 1,
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        child: Container(
                          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                            colors: [Colors.green[300], Colors.green],
                          ),),
                          child: ElevatedButton(
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>Hospitals()));
                            },
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                                ),
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.transparent
                                ),
                                elevation: MaterialStateProperty.all(0)
                            ),
                            child: Text("Nearest hospitals"),
                          ),
                        ),
                      ),
                    ],)
                        :

                    myStatus==2?  Column(children: [
                      Card(elevation: 11,color: Color(prospectColor),child: ListTile(contentPadding: EdgeInsets.all(10),leading: IconButton(onPressed: null, icon: Icon(Icons.warning,color: Colors.white,)),title: Text("You are a prospect please take the questionnaire ",style: TextStyle(color: Colors.white,fontSize: 15),),trailing: IconButton(onPressed: (){
                        popupQuestionnaire(context);
                      }, icon: Icon(FontAwesomeIcons.stethoscope,color: Colors.white,)),),),
                    ],)
                        :

                    prospectCount!=0?  Column(children: [
                      Card(elevation: 11,color: Color(prospectColor),child: ListTile(contentPadding: EdgeInsets.all(10),leading: IconButton(onPressed: null, icon: Icon(Icons.warning,color: Colors.white,)),title: Text("You have encountered prospect cases please take the questionnaire",style: TextStyle(color: Colors.white,fontSize: 15),),trailing: IconButton(onPressed: (){
                        popupQuestionnaire(context);
                      }, icon: Icon(FontAwesomeIcons.stethoscope,color: Colors.white,)),),),
                    ],)
                        :

                    unknownCount!=0?  Column(children: [
                      Card(elevation: 11,color: Color(unknownColor),child: ListTile(contentPadding: EdgeInsets.all(10),leading: IconButton(onPressed: null, icon: Icon(Icons.warning,color: Colors.white,)),title: Text("You have encountered unknown cases please take the questionnaire",style: TextStyle(color: Colors.white,fontSize: 15),),trailing: IconButton(onPressed: (){
                        popupQuestionnaire(context);
                      }, icon: Icon(FontAwesomeIcons.stethoscope,color: Colors.white,)),),),
                    ],)
                        :

                    Container(),

                    Image.asset("assets/images/radar.gif",height: 200,),
                    Card(elevation: 11,color: Color(0xFFFAFAFA),child: ListTile(title: Text("Total encountered : "+devicesAddresses.length.toString(),textAlign: TextAlign.center,style: TextStyle(color: Color(0xFF2c3e50),fontSize: 15),),),),
                    Row(children: [
                      Expanded(child: Card(color: Color(immuneColor),elevation: 11,child: ListTile(title: Text(immuneCount.toString(),textAlign: TextAlign.center,style: TextStyle(color: Colors.white),),subtitle: Text("Immune",textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 12),),),),),
                      Expanded(child: Card(color: Color(healthyColor),elevation: 11,child: ListTile(title: Text(healthyCount.toString(),textAlign: TextAlign.center,style: TextStyle(color: Colors.white),),subtitle: Text("Healthy",textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 12),),),),),
                    ],),
                    Row(children: [
                      Expanded(child: Card(color: Color(prospectColor),elevation: 11,child: ListTile(title: Text(prospectCount.toString(),textAlign: TextAlign.center,style: TextStyle(color: Colors.white),),subtitle: Text("Prospect",textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 12),),),),),
                      Expanded(child: Card(color: Color(infectedColor),elevation: 11,child: ListTile(title: Text(infectedCount.toString(),textAlign: TextAlign.center,style: TextStyle(color: Colors.white),),subtitle: Text("Infected",textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 12),),),),),
                    ],),
                    Card(color: Color(0xFFFAFAFA),elevation: 11,child: ListTile(title: Text("Unknown : "+unknownCount.toString(),textAlign: TextAlign.center,style: TextStyle(color: Color(unknownColor),fontSize: 15),)),),


                    SizedBox(height: 50,),

                  ],)
                ],);
              },
            ),

            StreamBuilder(
              stream: FirebaseFirestore.instance.collection('users').doc(myEmail).snapshots(),
              builder: (_, snapshot){
                return SingleChildScrollView(child: Column(children: [
                  Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [
                    Column(children: [
                      Icon(Icons.person,size: 130,color: Color(
                          snapshot.data['status']==0? unknownColor :
                          snapshot.data['status']==4? immuneColor :
                          snapshot.data['status']==1? healthyColor :
                          snapshot.data['status']==2? prospectColor :
                          snapshot.data['status']==3? infectedColor :
                          unknownColor),),
                      Text(snapshot.data['status']==0? "Unknown" : snapshot.data['status']==4? "Immune" : snapshot.data['status']==1? "Healthy" : snapshot.data['status']==2? "Prospect" : snapshot.data['status']==3? "Infected" : "", style: TextStyle(fontWeight: FontWeight.bold,color: Color(snapshot.data['status']==0? unknownColor : snapshot.data['status']==1? healthyColor : snapshot.data['status']==2? prospectColor : snapshot.data['status']==3? infectedColor : unknownColor)),),
                      SizedBox(height: 10,),
                    ],),
                    QrImage(
                      foregroundColor: Color(snapshot.data['status']==0? unknownColor : snapshot.data['status']==4? immuneColor : snapshot.data['status']==1? healthyColor : snapshot.data['status']==2? prospectColor : snapshot.data['status']==3? infectedColor : unknownColor),
                      data: snapshot.data['status']==0? "Unknown" : snapshot.data['status']==4? "Immune" : snapshot.data['status']==1? "Healthy" : snapshot.data['status']==2? "Prospect" : snapshot.data['status']==3? "Infected" : "" ,
                      version: QrVersions.auto,
                      size: 150,
                    ),
                  ],),
                  Card(child: ListTile(
                    leading: Icon(Icons.email),
                    title: Text(snapshot.data.id),
                  ),),
                  Card(child: ListTile(
                    leading: Icon(Icons.person),
                    title: Text(snapshot.data['name']),
                    trailing: IconButton(icon: Icon(FontAwesomeIcons.edit,color: Colors.green,size: 20,), onPressed: (){
                      popupEditName(snapshot.data.id ,snapshot.data['name'],nameTextEditingController,context);
                    }),
                  ),),
                  Card(child: ListTile(
                    leading: Icon(Icons.bluetooth),
                    title: Text(snapshot.data['bluetooth']),
                    trailing: IconButton(icon: Icon(FontAwesomeIcons.edit,color: Colors.green,size: 20,), onPressed: (){
                      popupEditBluetooth(snapshot.data.id ,snapshot.data['bluetooth'],bluetoothTextEditingController,context);
                    }),
                  ),),
                  Card(child: ListTile(
                    leading: Icon(Icons.phone_android),
                    title: Text(snapshot.data['phone']),
                    trailing: IconButton(icon: Icon(FontAwesomeIcons.edit,color: Colors.green,size: 20,), onPressed: (){
                      popupEditPhone(snapshot.data.id ,snapshot.data['phone'],phoneTextEditingController,context);
                    }),
                  ),),
                  Card(
                      child: Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(3)),gradient: LinearGradient(
                          colors: [Colors.green[300], Colors.green],
                        ),),
                        child: ListTile(
                          onTap: (){
                            areYouSure(context);
                          },
                          leading: Icon(Icons.logout,color: Colors.white,),
                          title: Text("Logout",style: TextStyle(color: Colors.white),),
                        ),
                      )),
                ],),);
              },
            ),
            Container(),
            SingleChildScrollView(child: Column(children: [
              Card(child: ListTile(
                title: Text("Active",textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFf1c40f)),),
                subtitle: Text(active,textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFf1c40f)),),
              ),),
              Row(children: [
                Expanded(child: Card(child: ListTile(
                  title: Text("Total cases",textAlign: TextAlign.center,),
                  subtitle: Text(cases,textAlign: TextAlign.center),
                ),),),
                Expanded(child: Card(child: ListTile(
                  title: Text("Today's cases",textAlign: TextAlign.center,),
                  subtitle: Text(todayCases,textAlign: TextAlign.center),
                ),),),
              ],),
              Row(children: [
                Expanded(child: Card(child: ListTile(
                  title: Text("Total deaths",textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFc0392b)),),
                  subtitle: Text(deaths,textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFc0392b)),),
                ),),),
                Expanded(child: Card(child: ListTile(
                  title: Text("Today's deaths",textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFc0392b)),),
                  subtitle: Text(todayDeaths,textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFc0392b)),),
                ),),),
              ],),
              Row(children: [
                Expanded(child: Card(child: ListTile(
                  title: Text("Critical",textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFe67e22)),),
                  subtitle: Text(critical,textAlign: TextAlign.center,style: TextStyle(color: Color(0xFFe67e22)),),
                ),),),
                Expanded(child: Card(child: ListTile(
                  title: Text("Recovered",textAlign: TextAlign.center,style: TextStyle(color: Color(0xFF2980b9)),),
                  subtitle: Text(recovered,textAlign: TextAlign.center,style: TextStyle(color: Color(0xFF2980b9)),),
                ),),),
              ],),
              Row(children: [
                Expanded(child: Card(child: ListTile(
                  title: Text("Tests",textAlign: TextAlign.center,),
                  subtitle: Text(tests,textAlign: TextAlign.center),
                ),),),
                Expanded(child: Card(child: ListTile(
                  title: Text("Affected countries",textAlign: TextAlign.center,),
                  subtitle: Text(affectedCountries,textAlign: TextAlign.center),
                ),),),
              ],),
            ],),),
            SingleChildScrollView(child: Column(children: [
              Image.asset("assets/images/1.jpg"),
              Image.asset("assets/images/2.png"),
              SizedBox(height: 85,)
            ],),)
          ],),
        snackBar: SnackBar(
          content: Text('Tap back again to exit',textAlign: TextAlign.center,),
        )
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.transparent,
        child: Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
              colors: [Colors.green[300], Colors.green],
            ),
          ),
          child: Icon(FontAwesomeIcons.stethoscope),
        ),
        onPressed: (){

          if(questionnaireTime!=null){
            DateTime dateNow = DateTime.parse(Timestamp.now().toDate().toString());
            DateTime dateQuestionnaire = DateTime.parse(questionnaireTime.toDate().toString());
            if(dateNow.difference(dateQuestionnaire).inDays<=14){
              Flushbar(
                flushbarPosition: FlushbarPosition.TOP,
                flushbarStyle: FlushbarStyle.GROUNDED,
                backgroundColor: Color(0xFFc0392b),
                messageText: Text("You already have taken the questionnaire please wait 14 days before you take it again",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
                duration:  Duration(seconds: 3),
              )..show(context);
            }else{
              popupQuestionnaire(context);
            }
          }else{
            popupQuestionnaire(context);
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        notchMargin: 6,
        clipBehavior: Clip.antiAlias,
        shape: CircularNotchedRectangle(),
        child: BottomNavigationBar(
          onTap: (index){setState(() {
            currentIndex=index;
          });
            controller.jumpToPage(index);

            if(index==3){
              getHttp();
            }
          },
          type: BottomNavigationBarType.fixed,
          currentIndex: currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(currentIndex==0? Icons.home : Icons.home_outlined),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Icon(currentIndex==1? Icons.account_circle : Icons.account_circle_outlined),
              label: "Profile",
            ),
            BottomNavigationBarItem(
              icon: Container(),
              label: "",
            ),
            BottomNavigationBarItem(
              icon: Icon(currentIndex==3? Icons.insert_chart : Icons.insert_chart_outlined),
              label: "Statistics",
            ),
            BottomNavigationBarItem(
              icon: Icon(currentIndex==4? Icons.integration_instructions_outlined : Icons.integration_instructions),
              label: "Instructions",
            )
          ],
        ),
      )
    );
  }
}