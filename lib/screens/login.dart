import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:covid19/global.dart';
import 'package:covid19/screens/functions/encrypt.dart';
import 'package:covid19/screens/loading.dart';
import 'package:covid19/screens/noInternet.dart';
import 'package:covid19/screens/register.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/custom_shape.dart';
import 'package:covid19/widgets/textformfield.dart';

class Login extends StatefulWidget{
  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> {

  bool passwordHidden=true;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey();
  List users=[];

  @override
  void initState() {
    super.initState();

  }

  saveEmail(String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('email', email);
  }

  final databaseReference = FirebaseFirestore.instance;

  void login() async{
    loading(context);
    databaseReference.collection("users").where(FieldPath.documentId,isEqualTo: emailController.text.toLowerCase()).get()
        .then((event) {
      if (event.docs.isNotEmpty) {
        if(decrypt(event.docs[0]['password'])==passwordController.text){
          saveEmail(event.docs[0].id.toLowerCase());
          myEmail=event.docs[0].id.toLowerCase();
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>Loading()), (route) => false);
        }else{
          Navigator.pop(context);
          Flushbar(
            flushbarPosition: FlushbarPosition.TOP,
            flushbarStyle: FlushbarStyle.GROUNDED,
            backgroundColor: Color(0xFFc0392b),
            messageText: Text("Email or password is wrong",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
            duration:  Duration(seconds: 3),
          )..show(context);
        }
      }else{
        Navigator.pop(context);
        Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          flushbarStyle: FlushbarStyle.GROUNDED,
          backgroundColor: Color(0xFFc0392b),
          messageText: Text("Email or password is wrong",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
          duration:  Duration(seconds: 3),
        )..show(context);
      }
    });
  }



  void validation(){
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailController.text);
    if(emailController.text.isEmpty){
      Flushbar(
        flushbarPosition: FlushbarPosition.TOP,
        flushbarStyle: FlushbarStyle.GROUNDED,
        backgroundColor: Color(0xFFc0392b),
        messageText: Text("Please enter your email",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
        duration:  Duration(seconds: 3),
      )..show(context);
    }else if(!emailValid){
      Flushbar(
        flushbarPosition: FlushbarPosition.TOP,
        flushbarStyle: FlushbarStyle.GROUNDED,
        backgroundColor: Color(0xFFc0392b),
        messageText: Text("Wrong email form",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
        duration:  Duration(seconds: 3),
      )..show(context);
    }else if(passwordController.text.isEmpty){
      Flushbar(
        flushbarPosition: FlushbarPosition.TOP,
        flushbarStyle: FlushbarStyle.GROUNDED,
        backgroundColor: Color(0xFFc0392b),
        messageText: Text("Please enter your password",style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
        duration:  Duration(seconds: 3),
      )..show(context);
    }else{
      checkInternet().then((value) => {
        if(value){
          login()
        }else{
          lastPage=Login(),
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>NoInternet()))
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DoubleBackToCloseApp(child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            clipShape(),
            Container(
              margin: EdgeInsets.only(left: 20, top: 50),
              child: Row(
                children: <Widget>[
                  Text(
                    "Welcome",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 40,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Row(
                children: <Widget>[
                  Text(
                    "Sign in to your account",
                    style: TextStyle(
                      fontWeight: FontWeight.w200,
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
            ),
            form(),
            SizedBox(height: 60),
            Material(
              elevation: 1,
              borderRadius: BorderRadius.all(Radius.circular(100)),
              child: Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)),gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),),
                child: ElevatedButton(
                  onPressed: (){
                    validation();
                  },
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
                      padding: MaterialStateProperty.all(
                          EdgeInsets.only(left: 50,right: 50,top: 15,bottom: 15)
                      ),
                      backgroundColor: MaterialStateProperty.all(
                          Colors.transparent
                      ),
                      elevation: MaterialStateProperty.all(0)
                  ),
                  child: Text("SIGN IN"),
                ),
              ),
            ),
            signUpTextRow(),
            SizedBox(height: 20,)
          ],
        ),
      ),snackBar: SnackBar(
        content: Text('Tap back again to exit',textAlign: TextAlign.center,),
      ),)
    );
  }

  Widget clipShape() {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.75,
          child: ClipPath(
            clipper: CustomShapeClipper(),
            child: Container(
              height: 220,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: ClipPath(
            clipper: CustomShapeClipper2(),
            child: Container(
              height: 170,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.green[300], Colors.green],
                ),
              ),
            ),
          ),
        ),
        Positioned.fill(child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: 150,
            height: 150,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 0.0,
                      color: Colors.black26,
                      offset: Offset(1.0, 10.0),
                      blurRadius: 20.0),
                ],
                color: Colors.white,
                shape: BoxShape.circle,
                image: DecorationImage(image: AssetImage('assets/images/logo.png'))
            ),
          ),
        ),)
      ],
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 40),
      child: Form(
        key: _key,
        child: Column(
          children: <Widget>[
            emailTextFormField(),
            SizedBox(height: 20),
            passwordTextFormField(),
          ],
        ),
      ),
    );
  }

  Widget emailTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.emailAddress,
      textEditingController: emailController,
      icon: Icons.email,
      hint: "Email",
    );

  }

  Widget passwordTextFormField() {
    return Stack(children: [
      CustomTextField(
        keyboardType: TextInputType.emailAddress,
        textEditingController: passwordController,
        icon: Icons.lock,
        obscureText: passwordHidden,
        hint: "Password",
      ),
      Positioned.fill(
        child: Align(
          alignment: Alignment.centerRight,
          child: IconButton(onPressed: (){

            if(passwordHidden){
              setState(() {
                passwordHidden=false;
              });
            }else{
              setState(() {
                passwordHidden=true;
              });
            }

          },icon: Icon(!passwordHidden ? Icons.visibility_outlined : Icons.visibility_off_outlined,size: 20,color: Colors.black54,),),
        ),
      )
    ],);
  }

  Widget signUpTextRow() {
    return Container(
      margin: EdgeInsets.only(top: 40),
      child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Register()));
            print("Routing to Sign up screen");
          },
          child: Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Don't have an account?",
                  style: TextStyle(fontWeight: FontWeight.w400,fontSize: 14,color: Colors.black87),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  "Sign up",
                  style: TextStyle(
                      fontWeight: FontWeight.w800, color: Colors.green[400], fontSize: 19),
                ),
              ],
            ),
          )
      ),
    );
  }
}