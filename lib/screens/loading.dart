import 'package:covid19/screens/home.dart';
import 'package:flutter/material.dart';

class Loading extends StatefulWidget{
  @override
  StateClass createState() => StateClass();
}

class StateClass extends State<Loading>{
  
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 1), () {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Home()));
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
          child: Container(
              color: Color(0xFFFAFAFA),
              alignment: Alignment.center,
              child: Center(child: Column(mainAxisSize: MainAxisSize.min,children: [
                Icon(Icons.check_circle_rounded,color: Colors.green,size: 70,),
                SizedBox(height: 5,),
                Text("Success",style: TextStyle(color: Colors.green,fontSize: 16),)
              ],),)
          )
      ),
    );
  }
}