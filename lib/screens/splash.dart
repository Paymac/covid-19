import 'dart:async';
import 'package:covid19/global.dart';
import 'package:covid19/screens/home.dart';
import 'package:covid19/screens/login.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  @override
  SplashState createState() => new SplashState();
}

class SplashState extends State<Splash>
    with SingleTickerProviderStateMixin {
  var _visible = true;

  AnimationController animationController;
  Animation<double> animation;

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.get('email')==null){
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Login()));
    }else{
      myEmail=prefs.get('email');
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Home()));
    }
  }

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 2));
    animation =
    new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: DoubleBackToCloseApp(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[

                Padding(padding: EdgeInsets.only(bottom: 30.0),child:new Image.asset('assets/images/logo.png',height: 25.0,fit: BoxFit.scaleDown,))


              ],),
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  'assets/images/logo.png',
                  width: animation.value * 300,
                  height: animation.value * 300,
                ),
              ],
            ),
          ],
        ),
        snackBar: SnackBar(
          content: Text('Tap back again to exit',textAlign: TextAlign.center,),
        ),
      ),
    );
  }
}